import json
from django.db import models
from django.conf import settings
from django.core.serializers import serialize

from django.db.models import fields

def upload_update_image(instance,filename):
  return "updates/{user}/{filename}".format(user = instance.user, filename = filename)

class UpdateQuerySet(models.QuerySet):
  def serialize(self):
    data = serialize('json', self)#, fields = ('user', 'content', 'image')) #!Uncomment previous comment to view only selected 3 fields
    data = [{**i['fields'],"id":i['pk']} for i in json.loads(data)]
    #list_values = list(self.values("id","user","content","image","update","timestamp"))
    
    return json.dumps(data) 
  
class UpdateManager(models.Manager):
  def get_queryset(self):
    return UpdateQuerySet(self.model, using=self._db)

class Update(models.Model):
  user    = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  content = models.TextField(blank= True, null=True)
  image   = models.ImageField(upload_to = upload_update_image, blank= True, null=True) 
  update = models.DateTimeField(auto_now=True)
  timestamp = models.DateTimeField(auto_now_add=True)
  
  objects = UpdateManager() 
  
  def __str__(self) -> str:
    return self.content or "Unnamed Update"
  
  #for single object, (for whole queryset you will need UpdateQuerySet Class)
  def serialize(self):
    json_data = serialize('json', [self])#, fields = ('user', 'content', 'image')) #!Uncomment previous comment to view only selected 3 fields
    struct = json.loads(json_data)
    data =  json.dumps({"id": struct[0]['pk'],**struct[0]['fields']})
    return data