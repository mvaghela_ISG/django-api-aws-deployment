from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.generic import View
from django.core.serializers import serialize
import json

from .models import Update
from DjangoRestCourse.Mixin import JsonResponseMixin

def updateModelDetailView(request):
  data = {
    "count": 1000,
    "content": "Some new Content"
  }
  return JsonResponse(data)
  
class JsonCBV(View):
  def get(self, request, *args, **kwargs):
    data = {
      "count": 1000,
      "content": "Some new Content"
    }
    return JsonResponse(data)
  
class JsonCBV2(JsonResponseMixin, View):
  def get(self, request, *args, **kwargs):
    data = {
      "count": 1000,
      "content": "Some new Content"
    }
    return self.renderToJsonResponse(data)

class SerializedDetailView(JsonResponseMixin, View):
  def get(self, request, *args, **kwargs):
    obj = Update.objects.get(id = 1)
    
    json_data = obj.serialize()
    return HttpResponse(json_data, content_type = 'application/json')
  

class SerializedListView(JsonResponseMixin, View):
  def get(self, request, *args, **kwargs):
    qSet = Update.objects.all() #Imports all the users
    #One way
    #data = serialize("json", qSet, fields = ('user','content'))
    
    #Another Way
    data = qSet.serialize() #Returns Json Object
    
    return HttpResponse(data, content_type = 'application/json')