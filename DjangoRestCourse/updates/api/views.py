from updates.models import Update as UpdateModel
from django.views.generic import View
from django.http import HttpResponse
import json

from updates.forms import UpdateModelForm
from .mixins import CSRFExemptMixin
from .utils import is_json

#? Retriving(Single Element), Creating, Updating, Deleting 
class UpdateModelDetailApiView(CSRFExemptMixin,View):
  '''
  Get(Retrive) Details,
  Update,
  Delete
  '''
  
  def get_object(self, id= None):
    qs = UpdateModel.objects.filter(id=id)
    if qs.count() == 1:
      return qs.first()
    return None
  
  def get(self, request, id, *args, **kwargs):
    obj = self.get_object(id)
    if obj == None:
      error_msg = json.dumps({"message":"No data exists for given id!"})
      return HttpResponse(error_msg, content_type = 'application/json',status = 404)
    json_data = obj.serialize()
    
    return HttpResponse(json_data, content_type = 'application/json', status =200) #json
    
  def post(self, request, id, *args, **kwargs):
    data = json.dumps({"message":"Not allowed to insert on a specific id, try without the number after the link!"})
    
    return HttpResponse(data, content_type = 'application/json', status = 400)
  
  def put(self, request, id, *args, **kwargs):
    #? Checking if Object Exists
    obj = self.get_object(id)
    if obj == None:
      error_msg = json.dumps({"message":"No data exists for given id!"})
      return HttpResponse(error_msg, content_type = 'application/json',status = 404)
    
    #? Validating received Data
    valid = is_json(request.body)
    if not valid:
      error_msg = json.dumps({"message":"Invalid data sent, Please Send Json Data!"})
      return HttpResponse(error_msg, content_type = 'application/json',status = 400)
    
    #? Updating the Object
    passed_data = json.loads(request.body)
    form = UpdateModelForm(passed_data, instance=obj) 
    if form.is_valid():
      obj = form.save(commit = True)
      obj_data = json.dumps({"message" : "Object Updating successful!"})
      return HttpResponse(obj_data, content_type = 'application/json', status = 201) #json
    if form.errors:
      data = json.dumps(form.errors)
      return HttpResponse(data, content_type = 'application/json', status = 400) #json
    
    data = json.dumps({"message" : "Update Request Received Successfully"})
    return HttpResponse(data, content_type = 'application/json', status = 200) #json
  
  def delete(self, request, id, *args, **kwargs):
    obj = self.get_object(id)
    if obj == None:
      error_msg = json.dumps({"message":"No data exists for given id!"})
      return HttpResponse(error_msg, content_type = 'application/json',status = 404)
    
    deleted = obj.delete()
    print(deleted)
    data = json.dumps({"message" : "Deleted the Object Successfully"})
    return HttpResponse(data, content_type = 'application/json', status = 200) #json
  
#For multiple model or requests without ids
class UpdateModelListApiView(CSRFExemptMixin,View):
  '''
  List views,
  Create View
  '''
  def get(self, request, *args, **kwargs):
    qs = UpdateModel.objects.all() 
    json_data = qs.serialize()
    return HttpResponse(json_data, content_type = 'application/json') #json
    
  def post(self, request, *args, **kwargs):
    
    valid = is_json(request.body)
    if not valid:
      error_msg = json.dumps({"message":"Invalid data sent, Please Send Json Data!"})
      return HttpResponse(error_msg, content_type = 'application/json',status = 400)
    
    data = json.loads(request.body)
    form = UpdateModelForm(data)
    if form.is_valid():
      obj = form.save(commit = True)
      obj_data = json.dumps({"message" : "Object creation successful!"})
      return HttpResponse(obj_data, content_type = 'application/json', status = 201) #json
    if form.errors:
      data = json.dumps(form.errors)
      return HttpResponse(data, content_type = 'application/json', status = 400) #json

    data = json.dumps({
      "Message" : "post request has no meaning"
    })
    return HttpResponse(data, content_type = 'application/json', status = 400) #json

  def put(self, request, *args, **kwargs):
    data = json.dumps({"message":"Can't Update All the objects, Provide Specific id!"})
    
    return HttpResponse(data, content_type = 'application/json', status = 400)
  
  def delete(self, request, *args, **kwargs):
    data = json.dumps({
      "Message" : "You cannot delete an entire list, Provide Specific id!"
    })
    return HttpResponse(data, content_type = 'application/json', status= 403) #json
