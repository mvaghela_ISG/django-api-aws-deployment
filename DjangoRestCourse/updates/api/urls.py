from django.urls import path

from .views import *

urlpatterns = [
    path('', UpdateModelListApiView.as_view()),
    path('<int:id>/', UpdateModelDetailApiView.as_view())
]
