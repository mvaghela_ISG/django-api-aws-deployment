from django.contrib import admin

from .models import Update as updateModel

admin.site.register(updateModel)