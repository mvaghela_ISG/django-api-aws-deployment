from django.http import JsonResponse
      
class JsonResponseMixin(object):
  def renderToJsonResponse(self,context, **response_kwargs):
    return JsonResponse(self.getdata(context), **response_kwargs)
  
  def getdata(self, context):
    return context
