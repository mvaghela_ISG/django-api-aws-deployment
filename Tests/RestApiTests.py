import requests
import json

BASE_URL = "http://127.0.0.1:8000/"

ENDPOINT = "api/updates/"

def get_list(id = ""):
  if id != "": r = requests.get(BASE_URL+ENDPOINT+f"{id}")
  else: r = requests.get(BASE_URL+ENDPOINT)
  return r.json()

def printData(id = ""):
  data = get_list(id)
  if type(data) is dict:
    for i in data.keys():
      print(str(i),":",data[i])
  else:
    num = 0
    for dt in data:
      print()
      for i in dt.keys():
        print(str(i),":",dt[i])

def createUpdate():
  user = input("User: ")
  if not user == "":
    content = input("Content: ")
    newData = {
      "user" : user,
      "content" : content
    }
  else:
    newData = {
      'user' : 1,
      'content' : 'Test from Outside'
    }
  r = requests.post(BASE_URL+ENDPOINT, data=json.dumps(newData))
  print("\nResponce Code:",r.status_code)
  
  try:print(r.json())
  except:print(r.text)
  
def doObjUpdate(id):
  user = input("User: ")
  if not user == "":
    content = input("Content: ")
    newData = {
      "user" : user,
      "content" : content
    }
  else:
    newData = {
      'user' : 1,
      'content' : 'Test from Outside'
    }
  r = requests.put(BASE_URL+ENDPOINT+f"{id}/", data=json.dumps(newData))
  print("\nResponce Code:",r.status_code)
  print(r.json())

def doObjDelete(id):
  r = requests.delete(BASE_URL+ENDPOINT+f"{id}/")
  print("\nResponce Code:",r.status_code)
  print(r.json())

if __name__ == "__main__":
  #createUpdate()
  doObjUpdate(1)
  #doObjDelete(1)
  #printData()